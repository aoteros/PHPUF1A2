<!DOCTYPE html>
<!--
Formulari de login que si existeix la cookie mostra un error.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <form action="processa_login.php" method="POST">
            Usuari <input type="text" name="usuari"/>
            Contrasenya <input type="password" name="passwd"/>
            <input type="submit" name="submit"/>
        </form>
        <?php 
        if (isset($_COOKIE["errors"]) && !empty($_COOKIE["errors"]) && $_COOKIE["errors"] != "OK") {
            echo "<h3>" . $_COOKIE["errors"] . "</h3>";
        }
        ?>       
    </body>
</html>
