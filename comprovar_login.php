<?php
/**
 * Comprova el login a partir de l'usuari afegit a la sessio
 */

    if (isset($_SESSION["usuari"]) && $_SESSION["usuari"] != "" && isset($_SESSION["passwd"]) && $_SESSION["passwd"] != "") {
        if ($arrayAssoc[$_SESSION["usuari"]] != $_SESSION["passwd"]) {
            $error = "L'usuari i la contrasenya no coincideixen.";
            setcookie("errors", $error, 0, '/');
            header("Location: formulari_login.php");
        }
    }else{
        $error = "Sisplau introdueixi una contrasenya i usuari. ";
        setcookie("errors", $error, 0, '/');
        header("Location: formulari_login.php");
    }

?>
