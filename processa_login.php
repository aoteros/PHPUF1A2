<?php
/**
 * Comproba si l'usuari esta a l'array. 
 * Si es aixi afegeix a la sessio i reenvia al menu.
 * Sino reenvia al formulari_login amb 2 errors diferents
 */
include 'array_associatiu.php';
$error = "";
$usuari = $_POST["usuari"];
if (isset($_POST["usuari"]) && $_POST["usuari"] != "" && isset($_POST["passwd"]) && $_POST["passwd"] != "") {
    if ($arrayAssoc[$_POST["usuari"]] == $_POST["passwd"]) {
        $arrayAssoc[$_POST["usuari"]] = $_POST["passwd"];
        session_start();
        $_SESSION["usuari"] = $_POST["usuari"];
        $_SESSION["passwd"] = $_POST["passwd"];
        header("Location: menu.php");
    }else{
        $error = "L'usuari i la contrasenya no coincideixen.";
        setcookie("errors", $error, 0, '/');
        header("Location: formulari_login.php");
    }
}else{
    $error = "Sisplau introdueixi una contrasenya i usuari. ";
    setcookie("errors", $error, 0, '/');
    header("Location: formulari_login.php");
}

?>