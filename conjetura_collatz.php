<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <h1>COMPROVAR DIVISORS</h1>
        <form action="conjetura_collatz.php" method="POST">
            Numero <input type="number" name="n"/>
            <input type="submit" name="submit"/>
        </form>
        <?php 
        
        session_start();
        
        include 'array_associatiu.php';
        
        include 'comprovar_login.php';
        
        if (isset($_POST["n"])) {
            collatz($_POST["n"]);
        }
        /**
         * Funcio que comproba la conjetura de collatz.
         * Continuara fins que la variable arribi a 1.
         * @param type $num
         */
        function collatz($num) {
            $printar = array();
            $count = 0;
            $maxNum = 0;
            while ($num != 1) {
                
                if ($num % 2 == 0) {
                   $num = $num / 2;
                }else{
                   $num = 3 * $num + 1;
                }
                
                array_push($printar, $num);
                
                if ($num>$maxNum) {
                    $maxNum = $num;
                }

                $count++;
            }

            echo "<p>La seqüència del " . $_POST["n"] . " és {";
            for ($index = 0; $index < count($printar); $index++) {
                
                if ($index == count($printar) - 1) {
                    
                    echo $printar[$index] . "}, després de " . $count . " iteracions i arribant a un màxim de " . $maxNum . ".</p>";

                }else{
                    
                    echo $printar[$index] . ", ";
                    
                }
            }
        }
        
        ?>       
        <a href="menu.php">Tornar al menu</a>
    </body>
</html>