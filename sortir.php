<?php
/**
 * Funcio sortir que treu les variables de sessio i la cookie.
 * Finalment acaba amb la sessio
 */
unset($_SESSION["usuari"]);
unset($_SESSION["passwd"]);
unset($_COOKIE["errors"]);
setcookie("errors", null, -1, '/');
session_abort();
header("Location: formulari_login.php");

?>