<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <h1>COMPROVAR DIVISORS</h1>
        <form action="comprovar_divisors.php" method="POST">
            Numero <input type="number" name="n"/>
            <input type="submit" name="submit"/>
        </form>
        <?php 
        
        
        session_start();
        
        include 'array_associatiu.php';
        
        include 'comprovar_login.php';
        
        if (isset($_POST["n"])) {
            primer($_POST["n"]);
        }
        /**
         * Funcio que comprobar si un numero es primer, 
         * si no es primer tambe mostra els seus divisors
         */
        function primer($num)
        {
            $printar = array();
            $cont = 0;
            $bool = true;

            for ($index = 2; $index <= $num; $index++) {
                if($num%$index==0)
                {
                    array_push($printar, $index);
                    $cont++;
                    if($cont>1){
                        $bool = false;
                    }               
                }
            }
            
            if (!$bool) {
                echo "<p>Sequencia del número: ";
                for ($index1 = 0; $index1 < count($printar); $index1++) {
                    if ($index1 == count($printar) - 1) {
                        echo $printar[$index1] . ".";
                    }else{
                        echo $printar[$index1] . ", ";
                    }
                }
                echo "</p>";
            }
            
            if ($bool) {
                echo "<p>El número " . $num . " és primer.</p>";
            }else{
                echo "<p>El número " . $num . " no és primer.</p>";
            }
        }
        ?>
        <a href="menu.php">Tornar al menu</a>
    </body>
</html>